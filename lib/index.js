'use strict';

var _ = require('lodash');

exports.register = function(server, options, next) {

    // Parsing the payload to convert from 'random-case' to 'camelCase'
    server.ext('onPostAuth', function(request, reply) {

        if(request.method === 'post'
            && request.route.settings.plugins['hapi-camelizer'] !== false
            && request.payload) {

            // Loop over the payload and convert to camelCase using the
            // lodash camelCase method (https://lodash.com/docs#camelCase)
            var camelizedPayload = _.reduce(request.payload, function(memo, val, key) {
                memo[_.camelCase(key)] = val;
                return memo;
            }, {});

            request.payload = camelizedPayload;
        }

        reply.continue();
    });

    return next();
};

exports.register.attributes = {
    pkg: require('../package.json')
};
