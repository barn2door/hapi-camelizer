'use strict';

var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.describe;
var it = lab.it;
var before = lab.before;
var expect = require('code').expect;

var Hapi = require('hapi');

describe('hapi-trailingslash', function () {

    var server;
    before(function(done) {
        server = new Hapi.Server();
        server.connection({ port: 3000});

        server.register(require('../'), function () {

            server.route({
                method: 'GET',
                path: '/',
                handler: function (request, reply) {
                    reply('Hi from the root!');
                }
            });

            server.route({
                method: 'POST',
                path: '/',
                handler: function (request, reply) {
                    reply(request.payload);
                }
            });

            server.route({
                method: 'POST',
                path: '/no-camelizer',
                config: {
                    plugins: {
                        'hapi-camelizer': false
                    }
                },
                handler: function (request, reply) {
                    reply(request.payload);
                }
            });

            done();
        });
    });

    it('should not do anything for a get', function (done) {

        server.inject({
            url: '/',
        }, function (response) {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.equal('Hi from the root!');
            done();
        });
    });

    it('should not do anything if there is no payload', function (done) {

        server.inject({
            url: '/',
            method: 'POST'
        }, function (response) {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.exist();
            expect(response.result).to.be.empty();
            done();
        });
    });

    it('should camelize the payload', function (done) {

        var object = {
            'test-key': 'hi!',
            camelCaseKey: 'hi!'
        };

        server.inject({
            url: '/',
            method: 'POST',
            payload: object
        }, function (response) {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.exist();
            expect(response.result).to.include({ testKey: 'hi!', camelCaseKey: 'hi!'});
            done();
        });
    });

    it('should do nothing if the route sets "hapi-camelizer" to false in plugin config', function (done) {

        var object = {
            'test-key': 'hi!',
            camelCaseKey: 'hi!'
        };

        server.inject({
            url: '/no-camelizer',
            method: 'POST',
            payload: object
        }, function (response) {
            expect(response.statusCode).to.equal(200);
            expect(response.result).to.exist();
            expect(response.result).to.include({ 'test-key': 'hi!', camelCaseKey: 'hi!'});
            done();
        });
    });
});
