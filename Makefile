test:
	@node node_modules/lab/bin/lab -vC
test-cov:
	@node node_modules/lab/bin/lab -vC -t 100
test-cov-html:
	@node node_modules/lab/bin/lab -vC -r html -o coverage.html && open coverage.html

.PHONY: test test-cov test-cov-html
